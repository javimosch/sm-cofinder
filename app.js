
import './components/sidebar'
import './components/appFooter'
import './components/skillsList'
import './components/usersList'

import Dashboard from './containers/dashboard'
import Profile from './containers/profile'

window.ERROR = {
    API: 'Erreur de serveur ou de connexion'
}
const routes = [
    { path: '/', component: Dashboard, name: 'dashboard' },
    { path: '/profile', component: Profile, name: 'profile' },
]
const router = new VueRouter({
    routes
})

// window.router = router
window.onLogout = () => {
    router.push({ name: 'logout' })
}
window.onLogin = () => {
    router.push({ name: 'dashboard' })

    api.funql({
        name: "pcun_registerUser"
    })
}

new Vue({
    name: 'app',
    el: '.app',
    router,
    data() {
        return {}
    },
    created() {
        console.log('APP created')
    }
})

console.log('APP')