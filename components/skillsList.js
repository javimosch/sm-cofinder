import { default as stylesMixin, template as stylesTpl } from '../mixins/styles'

Vue.component('skills-list', {
    mixins: [stylesMixin],
    props: [],
    template:
        stylesTpl(`
    <div class="skills_list" ref="root">
        <label>Skills</label>
        <div class="bl_list">
            <div class="bll_item"

            v-for="(item, index) in items" :key="item.id"    
            @click="removeSkill(item)"
            >
                <div class="blli_content">
                    <div class="blli_title" v-html="item.name"></div>
                </div>
            </div>
            <div class="bll_item"
                <input placeholder="EX: NodeJS" v-model="skillName" @change="saveSkill"/>
            </div>
        </div>
        <p v-if="false" v-show="items.length>0">Click an skill to remove it</p>
        <!-- <button class="btn" @click="refresh">Refresh skills</button>  -->
    </div>
</div>
`),
    destroyed() { },
    methods: {
        async saveSkill() {
            await api.funql({
                name: 'pcun_saveUserSkill',
                args: [{
                    name: this.skillName
                }]
            })
            this.skillName = ''
            this.refresh()
        },
        async removeSkill(item) {
            await api.funql({
                name: "pcun_remove",
                args: [{
                    table: 'user_skills',
                    field: 'id',
                    value: item.id
                }]
            })
            this.refresh()
        },
        async refresh() {
            this.items = await api.funql({
                name: "pcun_selectQuery",
                args: [{
                    query: `us.id, s.name FROM user_skills as us
                    LEFT JOIN skills as s on s.id = us.skill_id
                    WHERE user_id = ?`, queryArgs: ['PCUNUSERID']
                }]
            })
        }
    },
    computed: {},
    async mounted() {
        await this.refresh()
    },
    data() {
        return {
            skillName: '',
            items: [],
            styles: `
                p{
                    margin: 0px;
font-size: 10px;
font-style: italic;
text-align: left;
                }
                .skills_list{
                  margin:20px 0px;
                }
                .bl_list{
                    display: grid;
                    grid-template-columns: auto auto auto auto;
                }
                @media only screen and (max-width: 639px) {
                    .bl_list{
                        display: grid;
                        grid-template-columns: auto auto;
                    }
                }
                .bll_item{
                    cursor:pointer;
                    flex-direction: column;
                    display: block;
                    min-height: 30px;
                    min-width: 50px;
                    background-color: #522a55;
                    margin: 5px 5px 5px 0px;
                    border-radius: 20px;
                }
                .blli_content{
                    display:flex;
                    justify-content:center;
                    align-items:center;
                    color:white;
                    height: 100%;
                }
                .blli_title{
                    font-size: 12px;
padding: 8px;
                }
               
`
        }
    }
})