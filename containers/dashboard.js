import stylesMixin from '../mixins/styles'

export default {
    mixins: [stylesMixin],
    name: 'dashboard',
    props: [],
    template: `
    <div ref="scope">
        <div class="dashboard" ref="root" >
            <h2>Dashboard</h2>
            <label>Username filter</label>
            <input placeholder="steve54" v-model="filters.username" @change="refresh"/>
            <label>Skills filter</label>
            <input placeholder="Mana (4 letters minimum)" v-model="filters.skills" @change="refresh"/>
            <users-list ref="list" :filters="filters"></users-list>
        </div>
    </div>
    `,
    data() {
        var self = this
        return {
            filters:{
                username:'',
                skills:''
            },
            styles: `
            .dashboard{
            
            }
            
            @media only screen and (max-width: 639px) {
                
            }`
        }
    },
    computed: {

    },
    methods: {
        refresh(){
            if(this.filters.skills.length<4) return
            this.$refs.list.$emit('refresh')
        },
        async addBoard() {
            await api.funql({
                name: 'taskmeBoardAdd', args: [{
                    name: "BOARD TEST"
                }]
            })
        }
    },
    mounted() {

    }
}