module.exports = app => {
    var debug = require('debug')(`app:api:pcun_registerUser ${`${Date.now()}`.white}`)
    return async function pcun_registerUser(form) {
        var Moniker = require('moniker');
        var uniqid = require('uniqid');
        let username = uniqid(Moniker.choose()+`-`)
        return await app.dbExecute(
            `
INSERT INTO users
(email, username)
VALUES(?,?)
ON DUPLICATE KEY UPDATE
email = VALUES(email)
    `,
            [
                (this.user&&this.user.email)||form.email,
                username
            ],
            {
                dbName: this.dbName
            }
        )
    }
}