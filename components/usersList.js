import { default as stylesMixin, template as stylesTpl } from '../mixins/styles'

Vue.component('users-list', {
    mixins: [stylesMixin],
    props: ['filters'],
    template:
        stylesTpl(`
    <div class="users_list" ref="root">
        <button v-if="false" class="btn" @click="refresh">Refresh</button> 
        <div class="bl_list">
            <div class="bll_item"
            v-for="(item, index) in items" :key="item.id"    
            >
                <div class="blli_content">
                    <div class="blli_title" v-html="item.username"></div>
                    <div class="blli_title" v-html="item.skills"></div>
                </div>
            </div>
        </div>
    </div>
</div>
`),
    destroyed() { },
    methods: {
        async refresh() {
            var self = this
            this.items = await api.funql({
                name: "pcun_selectQuery",
                args: [{
                    filters: self.filters,
                    query: `u.username, IFNULL(GROUP_CONCAT(DISTINCT s.name SEPARATOR ' '),'') as skills
                    FROM users as u
                    LEFT JOIN user_skills as us on us.user_id = u.id
                    LEFT JOIN skills as s on s.id = us.skill_id
                    GROUP BY u.id
                    `, queryArgs: []
                }],
                transform: function(items){

                    if(!!args[0].filters && !!args[0].filters.username){
                        let filtersUsername = args[0].filters.username
                        items = items.filter(item=>{
                            if(item.skills.toLowerCase().indexOf(filtersUsername)!==-1){
                                return true
                            }else{
                                return false
                            }
                        })
                    }

                    if(!!args[0].filters && !!args[0].filters.skills){
                        let filterSkills = args[0].filters.skills
                        items = items.filter(item=>{
                            var parts = filterSkills.split(' ')
                            if(parts.filter(p=>item.skills.toLowerCase().indexOf(p)!==-1).length == parts.length){
                                return true
                            }else{
                                return false
                            }
                        })
                    }
                    return items;
                }
            })
        }
    },
    computed: {},
    async mounted() {
        //await this.refresh()
        this.$on('refresh', this.refresh)
    },
    data() {
        return {
            items: [],
            styles: `
                .users_list{
                  
                }
                .bl_list{
                    display: grid;
                    grid-template-columns: 1fr;
                }
                .bll_item{
                    flex-direction: column;
                    display: block;
                    padding: 5px;
                    background-color: #fafafa;
                    margin: 10px 10px 10px 0px;
                }
                .blli_content{
                    display: grid;
                    grid-template-columns: minmax(200px,1fr) minmax(200px,1fr);
                    max-width:100%;
                    overflow:auto;
                    max-height:400px;
                    color:#30426a;
                    height: 100%;
                }
                .blli_title{
                    font-size:18px;
                }
               
`
        }
    }
})