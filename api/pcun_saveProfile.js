module.exports = app => {
    var debug = require('debug')(`app:api:pcun_saveProfile ${`${Date.now()}`.white}`)
    return async function pcun_saveProfile(form) {
        return app.pcun_saveDocument(Object.assign({}, form, {
            id: this.pcunUser.id,
            _table: "users",
            _fields: ['username','firstname', 'lastname', 'age', 'phone'],
            _options: {
                dbName: this.dbName
            }
        }))
    }
}