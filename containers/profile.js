import { default as stylesMixin, template as stylesTpl } from '../mixins/styles'

export default {
    mixins: [stylesMixin],
    name: 'profile',
    props: [],
    template: stylesTpl(`
        <div class="profile" ref="root" >
            <h2>Profile</h2>
            <div class="ab_inputs">
                <div class="form_group">
                    <label>Email*</label>
                    <input :value="form.email" readonly disabled/>
                </div>
                <div class="form_group">
                    <label>Username</label>
                    <input v-model="form.username"/>
                </div>
                <div class="form_group">
                    <label>Firstname</label>
                    <input v-model="form.firstname"/>
                </div>
                <div class="form_group">
                    <label>Lastname</label>
                    <input v-model="form.lastname"/>
                </div>
                <div class="form_group">
                    <label>Age</label>
                    <input v-model="form.age"/>
                </div>
                <div class="form_group">
                    <label>Phone</label>
                    <input v-model="form.phone"/>
                </div>
                <skills-list></skills-list>
                <div class="form_group">
                    <input type="button" @click="!saving && save()" :disabled="saving" class="btn" :value="saveButtonText"/>
                </div>
            </div>
        </div>
    </div>
    `),
    data() {
        var self = this
        return {
            styles: `
            .btn:disabled{
                opacity:0.5;
            }
            
            .profile{
            
            }
            .ab_inputs{

            }
            .form_group{
                margin-top:30px;
            }
            .form_group label{
                min-width: 100px;
display: inline-block;
            }
            
            @media only screen and (max-width: 639px) {
                
            }`,
            form: {
                firstname: '',
                lastname: '',
                email: '',
                age: '',
                phone: ''
            },
            saving:false,
            saveButtonText:'Save'
        }
    },
    computed: {

    },
    methods: {
        async save() {
            this.saving = true
            
            await api.funql({
                name: 'pcun_saveProfile', args: [Object.assign({}, this.form)]
            })
            
            this.saveButtonText="Saved"
            setTimeout(()=>{
                this.saving=false
                this.saveButtonText="Save"
            },1000)
            /*
            this.$router.push({
                name: 'dashboard'
            })*/
        },
        async fetchDetails() {
            this.form = await api.funql({
                name: 'pcun_selectQuery',
                args: [{
                    query: `* FROM users WHERE id = ?`,
                    queryArgs: ['PCUNUSERID'],
                    queryOptions: {
                        single: true
                    }
                }]
            })
        }
    },
    mounted() {
        this.fetchDetails()
    }
}